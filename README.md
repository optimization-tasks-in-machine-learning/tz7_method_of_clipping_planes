# ТЗ7 Методы отсекающих плоскостей

Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине Оптимизационные задачи в машинном обучении. Для ознакомления с поставленной задачей нажмите [читать tz7_cutting_optimisation.pdf](illustrations/tz7_cutting_optimisation.pdf)

## Демонстрация возможностей

Множество примеров работы модуля доступны к просмотру любым удобным для Вас способом. <br/>
* Онлайн Google Colab Notebook - нажмите [открыть Google Colab](https://colab.research.google.com/drive/1xq4QsLuArkhftLJ1k3oU3eSMJiZddJYi?usp=sharing)
* Оффлайн Jupyter Notebook - нажмите [скачать tz7_cutting_optimisation.ipynb](tz7_cutting_optimisation.ipynb)

## Иллюстрация работы

* Решение и вывод <br/>
![Output](illustrations/output.png) <br/>
* Продвинутая визуализация <br/>
![Visualization](illustrations/visualization.gif) <br/>

## Подготовка окружения

```
# Для визуализации необходим CUDA
!nvidia-smi
!pip install GPUtil
```

## Импорт модуля

```
# Клонирование репозитория
!git clone https://leonidalekseev:maCRxpzWvEcohHKKTeG9@gitlab.com/optimization-tasks-in-machine-learning/tz7_cutting_optimisation.git
# Импорт всех функций из модуля
from tz7_cutting_optimisation.utils import *
```

## Документация функций

`help(set_function)`

```
Help on function set_function in module module tz7_cutting_optimisation.utils:

set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
    Установка объекта sympy функции из строки.
    Количество переменных проверяется. Отображение функции настраивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    functions_symbols: tuple, optional
        Переменные функции
    functions_title: str
        Заголовок для отображения
    functions_designation: str
        Обозначение для отображения
    is_display_input: bool
        Отображать входную функцию или нет
    _is_system: bool
        Вызов функции программой или нет
    
    Returns
    ===========
    
    input_investigated_function: str
        Входная строка с функцией
    investigated_function: sympy
        Исследуемая функция
    functions_symbols: tuple
        Переменные функции
```

`help(visualize_plotly)`

```
Help on function visualize_plotly in module tz7_cutting_optimisation.utils:

visualize_plotly(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, input_constraints: list = [], points_list: Union[tuple, NoneType] = None, is_display_input: bool = True, is_autosize_graph: bool = True, start: int = -1, stop: int = 1, detail: int = 100, step: Union[float, NoneType] = None) -> None
    Визуализация функции с точками. 
    Границы графика и детализация настроивается.
    
    Parameters
    ===========
    
    input_investigated_function: str
        Исследуемая функция
    input_constraints: list
        Набор ограничений типа равенств и неравенств
    functions_symbols: tuple, optional
        Переменные функции
    points_list: tuple, optional
        Точки для визуализации
    is_display_input: bool
        Отображать входную функцию или нет
    is_autosize_graph: bool
        Автоматические устанавливать границы или нет
    start: int
        Начало графика
    stop: int
        Конец графика
    detail: int
        Детализация графика
    step: float, optional
        Единичный отрезок графика
```

`help(cutting_optimization)`

```
Help on function cutting_optimization in module tz7_cutting_optimisation.utils:

cutting_optimization(method: Union[int, str], input_investigated_function: str, input_constraints: list, is_display_input: bool = True, is_display_solution: bool = True, is_save_solution: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
    Оптимизация с ограничениями с заданными параметрами. 
    
    Parameters
    ===========
    
    method: int, str
        Методы оптимизации
        0, 'gomori': метод Гомори
        1, 'branches_and_bound': метод ветвей и границ
    input_investigated_function: str
        Исследуемая функция
    input_constraints: list
        Набор ограничений типа равенств и неравенств
    is_display_input: bool = True
        Отображать входную функцию или нет
    is_display_solution: bool = True
        Отображать решение или нет
    is_save_solution: bool = False
        Сохранять решение или нет
    is_display_conclusion: bool = True
        Отображать вывод или нет               
    is_try_visualize: bool = False
        Визуализация процесса
    
    Returns
    ===========
    
    function_point: float
        Найденая точка экстремума
    function_value: float
        Значение функции в точке экстремума
    flag_process: int
        Флаг завершения алгоритма
        0: найдено значение с заданной точностью
        1: достигнуто максимальное количество итераций
        2: выполнено с ошибкой
```

## Участники проекта

* [Быханов Никита](https://gitlab.com/BNik2001) - Менеджер проектa, Тестировщик
* [Алексеев Леонид](https://gitlab.com/LeonidAlekseev) - Программист, Тестировщик
* [Семёнова Полина](https://gitlab.com/polli_eee) - Аналитик, Тестировщик
* [Янина Марина](https://gitlab.com/marinatdd) - Аналитик, Тестировщик
* [Буркина Елизавета](https://gitlab.com/lizaburkina) - Аналитик
* [Егорин Никита](https://gitlab.com/hadesm8) - Аналитик

`Группа ПМ19-1`

## Используемые источники

* [Sympy documentation](https://www.sympy.org/en/index.html)
* [Plotly documentation](https://plotly.com/python/)

